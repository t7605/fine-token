# Deploy

1. Paste private key in file .env
2. Change config in file deploy/deployToken.js
3. Deploy token
    - Deploy testnet
    ```shell
       yarn run deploy-testnet-bsc
    ```
    - Deploy mainnet
    ```shell
       yarn run deploy-mainnet-bsc
    ```
   - Deploy ethereum
    ```shell
       yarn run deploy-mainnet-ethereum
    ```
4. Verify token
    - Testnet
    ```shell
       yarn run verify-testnet-bsc
    ```
    - Mainnet
      ```shell
         yarn run verify-mainnet-bsc
      ```
   - Mainnet
     ```shell
        yarn run verify-mainnet-ethereum
     ```  
# Flow 
1. Deploy contract
2. Verify contract
3. Add liquidity
4. Enable trading: call function `enableTrading`
5. Enable or disable fee:
   - Call function: `enableOrDisableFees`
