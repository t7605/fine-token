require("@nomicfoundation/hardhat-toolbox");

/** @type import('hardhat/config').HardhatUserConfig */
const path = require("path");

require("hardhat-deploy");
require("hardhat-deploy-ethers");
require("hardhat-contract-sizer");
const dotenv = require('dotenv');
const {HardhatUserConfig} = require('hardhat/types');


//load env
console.log('env_path',path.resolve(__dirname, '.env'));
dotenv.config({ path:  path.resolve(__dirname, '.env')});
console.log(process.env.PRIVATE_KEY)

const config = {
  solidity: {
    compilers: [
      {
        version: '0.6.12',
        settings: {
          optimizer: {
            enabled: true,
            runs: 2000,
          },
        },
      },
      {
        version: '0.4.26',
        settings: {
          optimizer: {
            enabled: true,
            runs: 2000,
          },
        },
      },
      {
        version: '0.8.18',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      }
    ],
  },
  networks: {
    localhost: {
      url: 'http://localhost:8545',
      accounts: [`${process.env.PRIVATE_KEY}`],
      gas : 20e12,
      allowUnlimitedContractSize: true

    },
    bsctestnet: {
      url: 'https://data-seed-prebsc-2-s2.binance.org:8545/',
      accounts: [`${process.env.PRIVATE_KEY}`],
      live: true
    },
    bsc: {
      url: 'https://bsc-dataseed.binance.org',
      accounts: [`${process.env.PRIVATE_KEY}`],
      live: true,
    },
    matic: {
      url: 'https://rpc-mainnet.maticvigil.com',
      accounts: [`${process.env.PRIVATE_KEY}`],
      live: true,
      gasPrice: 20e9
    },
    test: {
      url: 'https://rpc-mainnet.maticvigil.com',
      accounts: [`${process.env.PRIVATE_KEY}`],
      live: true,
      gasPrice: 20e9
    },
    mumbai: {
      url: 'https://rpc-mumbai.maticvigil.com',
      accounts: [`${process.env.PRIVATE_KEY}`],
      live: true,
      gasPrice: 1e9
    },
    fuji: {
      url: 'https://api.avax-test.network/ext/bc/C/rpc',
      gasPrice: 225000000000,
      chainId: 43113,
      accounts: [`${process.env.PRIVATE_KEY}`]
    },
    avalanche: {
      url: 'https://api.avax.network/ext/bc/C/rpc',
      gasPrice: 40000000000,
      chainId: 43114,
      accounts: [`${process.env.PRIVATE_KEY}`]
    },
    ethereum: {
      url: 'https://rpc.ankr.com/eth\t',
      chainId: 1,
      accounts: [`${process.env.PRIVATE_KEY}`],
    },
    goerli: {
      url: 'https://rpc.ankr.com/eth_goerli\t',
      chainId: 5,
      accounts: [`${process.env.PRIVATE_KEY}`]
    }
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: process.env.API_KEY
  },
  gasReporter: {
    currency: 'USD',
    gasPrice: 5,
    enabled: !!process.env.REPORT_GAS,
  },
  namedAccounts: {
    test: 0,
    creator: 0,
    deployer: 0,
  }
};

module.exports = config