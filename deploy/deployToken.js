const {parseUnits} = require('ethers');

module.exports = async ({getNamedAccounts, deployments}) => {
    const {deploy, execute} = deployments;
    const {deployer} = await getNamedAccounts();
    console.log("deployer", deployer)
    // address mkt_, address dexRouter_, uint256 supply_, string name_, string symbol_
    const mkt = '0xCB3cCC9c020494B6285747ceaf65ec3cD1729C43'
    // const dexRouter = '0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3' // testnet
    const dexRouter = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D' // goerli
    // const dexRouter = '0x10ED43C718714eb63d5aA57B78B54704E256024E' // mainnet
    const supply = parseUnits('100000000', 18)
    const name = 'TEST'
    const symbol = 'TST'
    const token = await deploy('Token', {
        from: deployer,
        args: [mkt, dexRouter, supply, name, symbol],
        log: true,
    });
    console.log(token.address);



};
module.exports.tags = ['token'];